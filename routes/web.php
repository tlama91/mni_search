<?php
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function (Request $request) {
    $email = $request->session()->get('email', null);
    $password = $request->session()->get('password', null);
    return view('login', ['email' => $email, 'password' => $password]);
})->middleware('notLoggedIn');

Route::get('login', function (Request $request) {
    $email = $request->session()->get('email', null);
    $password = $request->session()->get('password', null);
    return view('login', ['email' => $email, 'password' => $password]);
})->middleware('notLoggedIn');

Route::post('authenticate', function () {
    return view('home');
})->middleware('login');

// routes that require user to have been authenticated
Route::group(['middleware' => 'checkToken'], function () {
    Route::get('home', function () {
        return view('home');
    });

    Route::get('profile', function () {
        return view('profile');
    });

    Route::post('resetPassword', 'PasswordController@postReset');

    Route::post('forgotPassword', 'PasswordController@postForgot');

    Route::get('logout', function (Request $request) {
        $email = $request->session()->get('email', null);
        $password = $request->session()->get('password', null);
        return view('login', ['email' => $email, 'password' => $password]);
    })->middleware('logout');
});
