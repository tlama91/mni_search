<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Mail\PasswordReset;
use Firebase\JWT\JWT;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Session;

class PasswordController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function postReset(Request $request)
    {
        //instantiate the client
        $client = new Client();
        $decoded = JWT::decode(Session::get('token'), env('JWT_SECRET'), ['HS256']);
        $decoded_array = (array) $decoded;
        $userInfo = (array) $decoded_array['user'];
        $email = $userInfo['email'];
        try {
            $result = $client->post('https://auth.adready.com/user/changePassword', [
                'json' => [
                    'userEmail' => $email,
                    'newPassword' => $request->input('newPassword1'),
                    'confirmPassword' => $request->input('newPassword2'),
                ],
            ]);
            $response = (array) json_decode($result->getBody());
            $response = $response['response'];
            $token = $response->data->token;
            session(['token' => $token]);
        } catch (\Exception $e) {
            dd($e);
        }

        return view('/');
    }

    public function postForgot(Request $request)
    {
        \Mail::to($request->input('email'))
            ->send(new PasswordReset($request->input('email'), md5($request->input('email'))));

        return view('/')->with(['message' => 'Email sent']);
    }
}
