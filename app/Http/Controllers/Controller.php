<?php

namespace App\Http\Controllers;

use App\Services\SearchService;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $searchService;

    public function __construct()
    {
        $this->searchService = new SearchService();
    }

    public function search(Request $request)
    {
        $query = $request->input('query') ?? '';
        $size = $request->input('size') ?? 300;
        $from = $request->input('from') ?? 0;
        $providerId = $request->input('providerId') ?? [];

        return (!is_string($query) || strlen($query) < 3) ? [] : $this->searchService->query($query, $size, $from, $providerId);
    }
}
