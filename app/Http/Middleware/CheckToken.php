<?php

namespace App\Http\Middleware;

use Closure;
use Firebase\JWT\JWT;

class CheckToken
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        JWT::$leeway = 6000;
        // Obtain token from storage
        $token = $request->session()->get('token', null);
        // Continue with request if token exists
        if ($token != null) {
            // $decodedArray = (array) JWT::decode($token, env('JWT_SECRET'), ['HS256']);
            // $userInfo = (array) $decodedArray['user'];
            // $request->attributes->add(['userInfo' => $userInfo]);
            return $next($request);
        }
        // Else redirect to login
        return redirect('login');
    }
}
