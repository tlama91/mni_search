<?php
/**
 * Created by PhpStorm.
 * User: bkim
 * Date: 10/26/17
 * Time: 3:05 PM
 */

namespace App\Services;


use FantasticSearch\FantasticSearch;
use Storage;

class ElasticSearchService
{
    private $fs;
    private $indexName = 'mni_segments';
    private $typeName = 'mni_segment';

    private $fieldNames = [
        'short_name',
        'description',
    ];

    private $synonyms = [
        'mother,mom,mommy,ma,mama',
        'father,dad,daddy,pa,papa',
        'hhi,household income',
        'women,w,f',
        'men,m',
        'datalogix,DLX',
        'nielsen catalina segments,nielsen segments,catalina segments,ncs',
        'dun and bradstreet,d&b',
        'business to business,b2b',
        'business to consumer,b2c',
        'sports,sport,basketball,baseball,football,soccer,nfl,golf,hockey',
        'college sports,ncaa',
    ];

    public function __construct()
    {
        ini_set('memory_limit', '-1');
        $this->fs = new FantasticSearch(env('ELASTIC_HOST'), env('ELASTIC_PORT'));
    }

    public function setup()
    {
        // remove an existing mni_segments index
        $this->fs->deleteIndex($this->indexName);

        // create a new mni_segments index
        $this->createMNISegmentsIndex();

        // index real data
        $this->indexSegmentNames();
    }

    private function createMNISegmentsIndex()
    {
        $indexSettings = [
            'analysis' => [
                'filter'   => [
                    'synonyms_filter'            => [
                        'type'     => 'synonym',
                        'synonyms' => $this->synonyms,
                    ],
                    'stop_words_filter'          => [
                        'type'            => 'stop',
                        'stopwords'       => '_english_',
                        'remove_trailing' => 'false',
                        'ignore_case'     => 'true',
                    ],
                    'english_stemmer'            => [
                        'type'     => 'stemmer',
                        'language' => 'english',
                    ],
                    'english_possessive_stemmer' => [
                        'type'     => 'stemmer',
                        'language' => 'possessive_english',
                    ],
                ],
                'analyzer' => [
                    'synonyms_analyzer' => [
                        'tokenizer' => 'standard',
                        'filter'    => [
                            'lowercase',
                            'synonyms_filter',
                            'stop_words_filter',
                            'asciifolding',
                            'english_stemmer',
                            'english_possessive_stemmer',
                        ],
                    ],
                ],
            ],
        ];

        $indexMappings = ['_default_' => ['properties' => []]];

        foreach ($this->fieldNames as $fieldName) {
            $indexMappings['_default_']['properties'][$fieldName] = [
                'type'   => 'text',
                'fields' => [
                    'exact'   => [
                        'type' => 'text',
                    ],
                    'synonym' => [
                        'type'     => 'text',
                        'analyzer' => 'synonyms_analyzer',
                    ],
                ],
            ];
        }

        $this->fs->createIndex($this->indexName, $indexSettings, $indexMappings);
    }

    private function indexSegmentNames()
    {
//        $date = date('Y-m-d');
        $date = '2017-11-05';
        $s3dir ='data/AppNexus/AppNexus/454/';
        $segmentFiles = Storage::disk('s3')->files($s3dir);

        $filteredSegmentFiles = array_filter($segmentFiles, function ($segmentFile) use ($date) {
            $matched = preg_match('/^.*\/(\d{4}-\d{2}-\d{2})_segments_(\w+).*\.json$/', $segmentFile, $matches);

            return ($matched && ($matches[1] === $date) && $matches[2] != 'whole');
        });

        foreach ($filteredSegmentFiles as $segmentFile) {
            $segments = json_decode(Storage::disk('s3')->get($segmentFile),true);

            // remove segments without a category
            foreach ($segments as $index => $segment) {
                if (empty($segment['short_name']) && empty($segment['description'])) {
                    unset($segments[$index]);
                    continue;
                }

                $segments[$index] = [
                    'id'          => $segment['id'],
                    'short_name'  => $segment['short_name'],
                    'description' => $segment['description'],
                    'member_id'   => $segment['member_id'] ?? 0,
                ];
            }

            $this->fs->setIndexType($this->indexName, $this->typeName);
            $this->fs->indexDocuments($segments);
        }
    }
}