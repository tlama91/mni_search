<?php
/**
 * Created by PhpStorm.
 * User: bkim
 * Date: 10/23/17
 * Time: 11:24 AM
 */

namespace App\Services;

use FantasticSearch\FantasticSearch;

class SearchService
{
    private $fs;

    private $indexName = 'mni_segments';
    private $typeName = 'mni_segment';

    private $fieldsToQuery = [
        'short_name',
        'description',
    ];

    public function __construct()
    {
        $this->fs = new FantasticSearch(env('ELASTIC_HOST'), env('ELASTIC_PORT'));
    }

    public function query(string $queryValue = '', int $size = 300, $from = 0, $providerId = [])
    {
        // build the query object
        $queryObject = ['query' => ['bool' => ['should' => []]], 'size' => $size, 'from' => $from, 'explain' => false];

        foreach ($this->fieldsToQuery as $field) {
            // match exact complete query value
            $queryObject['query']['bool']['should'][] = [
                'match' => [
                    "$field.exact" => [
                        'query' => $queryValue,
                        'type' => 'phrase',
                        'boost' => 10,
                    ],
                ],
            ];

            // match with synonyms, and stems
            $queryObject['query']['bool']['should'][] = [
                'match' => [
                    "$field.synonym" => [
                        'query' => $queryValue,
                        'boost' => 4,
                    ],
                ],
            ];

            // match with typos
            $queryObject['query']['bool']['should'][] = [
                'match' => [
                    "$field.synonym" => [
                        'query' => $queryValue,
                        'fuzziness' => 'AUTO',
                        'boost' => 2,
                    ],
                ],
            ];

            // for each term in the query value
            foreach (explode(' ', $queryValue) as $queryTerm) {
                // match exact terms
                $queryObject['query']['bool']['should'][] = [
                    'term' => [
                        "$field.exact" => [
                            'value' => $queryTerm,
                            'boost' => 4,
                        ],
                    ],
                ];

                // match partial with regex
                $queryObject['query']['bool']['should'][] = [
                    'regexp' => [
                        "$field.exact" => [
                            'value' => ".*$queryTerm.*",
                            'boost' => 2,
                        ],
                    ],
                ];
            }
        }

        // filter results by provider (member id)
        if (!empty($providerId)) {
            if (!isset($queryObject['query']['bool']['filter'])) {
                $queryObject['query']['bool']['filter'] = [];
            }

            $queryObject['query']['bool']['filter'][] = [
                'terms' => [
                    'member_id' => $providerId,
                ],
            ];
        }

        $searchResponse = $this->fs->search('GET', "$this->indexName/$this->typeName/_search", $queryObject);

        // if error or no result
        if (!isset($searchResponse['hits']['hits']) || empty($searchResponse['hits']['hits'])) {
            return [];
        }

        // return the query result
        return array_map(function ($hit) {
            return $hit['_source']['short_name'];
        }, $searchResponse['hits']['hits']);
    }
}
