<?php

namespace App\Console\Commands;

use App\Services\ElasticSearchService;
use Illuminate\Console\Command;

class DummyCommand extends Command
{
    protected $signature = 'x';
    protected $description = '';

    public function handle()
    {
        ini_set('memory_limit', '-1');
        $ess = new ElasticSearchService();
        $ess->setup();
    }
}