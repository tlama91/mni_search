import Vue from 'vue';
import Vuex from 'vuex';
import store from './store/index';

Vue.use(Vuex);

Vue.component('Search', require('./components/SearchTool.vue'));

const app = new Vue({
    el: '#vue-app',
    store,
});
