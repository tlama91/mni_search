import Vue from 'vue';
import Vuex from 'vuex';
import searchToolState from './modules/searchToolState';
import segmentName from './modules/segmentName';
import selectedSegmentName from './modules/selectedSegmentName';

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        searchToolState,
        segmentName,
        selectedSegmentName,
    }
});
