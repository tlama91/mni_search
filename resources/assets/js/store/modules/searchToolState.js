const state = {
    showResults: false,
    providers: [
        {providerName: 'eXelate', providerId: [39]},
        {providerName: 'Datonics', providerId: [90]},
        {providerName: 'AdAdvisor', providerId: [106]},
        {providerName: 'LinkedIn Corporation', providerId: [232]},
        {providerName: 'Lotame', providerId: [422]},
        {providerName: 'Peer39', providerId: [517]},
        {providerName: 'comScore Inc.', providerId: [772]},
        {providerName: 'ShareThis Inc.', providerId: [888]},
        {providerName: 'I-Behavior', providerId: [957]},
        {providerName: 'Grapeshot', providerId: [1133]},
        {providerName: 'DoubleVerify', providerId: [1182]},
        {providerName: 'Cross Pixel Media', providerId: [1218]},
        {providerName: 'BlueKai 3rd Party Data', providerId: [1262]},
        {providerName: 'Integral Ad Science', providerId: [1296, 3520]},
        {providerName: 'ADmantX', providerId: [1306]},
        {providerName: 'Eyeota', providerId: [1706]},
        {providerName: 'BlueKai 1st Party Data', providerId: [1781]},
        {providerName: 'VisualDNA', providerId: [2479]},
        {providerName: 'Free Stream Media Corp.', providerId: [2511]},
        {providerName: 'Statiq Ltd', providerId: [2682]},
        {providerName: 'AdDaptive Intelligence, Inc.', providerId: [3013]},
        {providerName: 'Skimlinks, Inc.', providerId: [3801]},
        {providerName: 'Audiens S.R.L.', providerId: [3847]},
        {providerName: 'Gourmet Ads Pty Limited', providerId: [4018]},
        {providerName: 'Qriously Ltd.', providerId: [4040]},
        {providerName: 'Retargetly', providerId: [4054]},
        {providerName: 'Skyhook Wireless Inc', providerId: [4068]},
        {providerName: 'Chegg, Inc.', providerId: [6930]},
        {providerName: 'WhiteOps, Inc.', providerId: [7074]},
        {providerName: 'Cardlytics, Inc.', providerId: [7162]},
        {providerName: 'LiveRamp Data Store', providerId: [8082]},
    ],
    providerSearchSelected: false,
    providerId: [],
    providerName: '',
};

const getters = {
    showResults: state => state.showResults,
    providers: state => state.providers,
    providerNames: state => state.providers.map(provider => provider.providerName),
    providerIDs: state => state.providers.map(provider => provider.providerId),
    providerSearchSelected: state => state.providerSearchSelected,
    keywordSearchSelected: state => !state.providerSearchSelected,
    providerId: state => state.providerId,
    providerName: state => state.providerName,
};

const mutations = {
    switchShowResults: (state, showResults) => {
        state.showResults = (!showResults && typeof showResults === 'object') ? !state.showResults : showResults;
    },
    toggleProviderSearchSelected: state => state.providerSearchSelected = !state.providerSearchSelected,
    setProvider: (state, provider) => {
        state.providerId = provider.providerId;
        state.providerName = provider.providerName;
    },
};

const actions = {
    switchShowResults({commit}, showResults) {
        commit('switchShowResults', showResults);
    },
    toggleProviderSearchSelected({commit}) {
        commit('toggleProviderSearchSelected');
    },
    setProvider({commit}, provider) {
        commit('setProvider', provider);
    },
};

export default {
    state,
    getters,
    mutations,
    actions,
};