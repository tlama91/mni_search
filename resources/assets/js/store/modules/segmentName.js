import axios from 'axios';

const state = {
    namesPerPage: 30,
    segmentNames: [],
    querySize: 300,
    query: '',
};

const getters = {
    namesPerPage: state => state.namesPerPage,
    segmentNames: state => state.segmentNames,
    querySize: state => state.querySize,
    query: state => state.query,
};

const mutations = {
    setNamesPerPage: (state, namesPerPage) => state.namesPerPage = namesPerPage,
    setSegmentNames: (state, segmentNames) => state.segmentNames = segmentNames,
    setQuerySize: (state, newQuerySize) => state.querySize = newQuerySize,
    setQuery: (state, query) => state.query = query,
    addAdditionalSegmentNames: (state, additionalSegmentNames) => state.segmentNames = state.segmentNames.concat(additionalSegmentNames),
};

const actions = {
    setNamesPerPage({commit}, namesPerPage) {
        commit('setNamesPerPage', namesPerPage);
    },
    setQuery({commit}, query) {
        commit('setQuery', query);
    },
    setQuerySize({commit}, newQuerySize) {
        commit('setQuerySize', newQuerySize);
    },
    setSegmentNames({commit, state, rootState}, query) {
        const params = {
            query,
            size: state.querySize,
            providerId: rootState.searchToolState.providerId,
        };

        commit('setQuery', query);
        axios.get('api/', {params})
            .then((response) => {
                commit('setSegmentNames', response.data);
            })
            .catch(error => error);
    },
    loadMoreSegmentNames({commit, state, rootState}) {
        const params = {
            query: state.query,
            size: state.querySize,
            from: state.segmentNames.length,
            providerId: rootState.searchToolState.providerId,
        };

        axios.get('api/', {params})
            .then((response) => {
                commit('addAdditionalSegmentNames', response.data);
            })
            .catch(error => error);
    },
};

export default {
    state,
    getters,
    mutations,
    actions,
};