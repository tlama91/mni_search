const state = {
    selectedSegmentNames: [],
};

const getters = {
    selectedSegmentNames: state => state.selectedSegmentNames,
};

const mutations = {
    addToSelected: (state, segmentNames) => state.selectedSegmentNames = state.selectedSegmentNames.concat(segmentNames),
    removeFromSelected: (state, index) => state.selectedSegmentNames.splice(index, 1),
    removeAllSelected: state => state.selectedSegmentNames = [],
};

const actions = {
    addToSelected: ({commit}, segmentNames) => commit('addToSelected', segmentNames),
    removeFromSelected: ({commit}, segmentName) => commit('removeFromSelected', segmentName),
    removeAllSelected: ({commit}) => commit('removeAllSelected'),
};

export default {
    state,
    getters,
    mutations,
    actions,
};