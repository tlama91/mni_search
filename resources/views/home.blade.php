<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>MNI Search Tool</title>
        <link rel="stylesheet" href="{{ asset('/css/home.css') }}">
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    </head>
    <body>
      <div class="nav-container">
        <div class="nav-bar">
          <nav class="logo" id="desktop"><a href="home"><img src="{{ asset('/images/mni.png') }}"></a></nav>
          <nav class="logo" id="mobile"><a href="home"><img src="{{ asset('/images/logo.png') }}"></a></nav>
          <nav class="nav-button"><a href="logout"><img src="{{ asset('/images/logout.png') }}"></a></nav>
          <nav class="nav-button"><a href="profile"><img src="{{ asset('/images/profile.png') }}"></a></nav>
        </div>
        <hr>
      </div>
      <div id="vue-app">
          <Search></Search>
      </div>
    </body>
    <script type="text/javascript" src="{{asset('js/app.js')}}"></script>
</html>
