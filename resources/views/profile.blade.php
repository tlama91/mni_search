<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
      <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Login</title>
        <link rel="stylesheet" href="{{ asset('/css/profile.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/home.css') }}">
      </head>
      <body>
        <div class="nav-container">
          <div class="nav-bar">
            <nav class="logo" id="desktop"><a href="home"><img src="{{ asset('/images/mni.png') }}"></a></nav>
            <nav class="logo" id="mobile"><a href="home"><img src="{{ asset('/images/logo.png') }}"></a></nav>
            <nav class="nav-button"><a href="logout"><img src="{{ asset('/images/logout.png') }}"></a></nav>
            <nav class="nav-button"><a href="profile"><img src="{{ asset('/images/profile.png') }}"></a></nav>
          </div>
          <hr>
        </div>
        <div class="form-container">
          {{Form::open (array ('url' => 'resetPassword'))}}
          <h2>Password Reset</h2>
          <p> {{Form::password ('oldPassword', array('placeholder'=>'Enter old password','maxlength'=>30)) }} </p>
          <p> {{Form::password ('newPassword1', array('placeholder'=>'New password','maxlength'=>30)) }} </p>
          <p> {{Form::password ('newPassword2', array('placeholder'=>'Confirm new password','maxlength'=>30)) }} </p>
          <p> {{Form::submit ('RESET PASSWORD')}} </p>
          {{Form::close ()}}

          @if ($errors->any ())
            <div class="errors">
              <h2>Errors</h2>
               @foreach ($errors->all() as $error)
                  <ul>
                    <li>{{ $error }}</li>
                  </ul>
              @endforeach
            </div>
          @endif
        </div>
      </body>
</html>
