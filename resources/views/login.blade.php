<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
      <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Login</title>
        <link href="{{ asset('/css/login.css') }}" rel="stylesheet">
      </head>
      <body>
          <div class="container">
            <img src="{{ asset('/images/mni.png') }}">
            {{-- You can set authentication parameters here --}}
            {{Form::open (array ('url' => 'authenticate'))}}
            @if($email)
              <p class="login-input"> {{Form::email ('email', $email, array ('maxlength'=>30))}} </p>
              <p class="login-input"> {{Form::input('password', 'password', $password) }} </p>
            @else
              <p class="login-input"> {{Form::email ('email', '', array ('placeholder'=>'Email','maxlength'=>30))}} </p>
              <p class="login-input"> {{Form::password ('password', array('placeholder'=>'Password','maxlength'=>30)) }} </p>
            @endif
            <p class="login-btn"> {{Form::submit ('Login')}} </p>
            <p class="remember"> {{Form::checkbox('rememberMe', true)}} Remember Me <p>
            <p class="options forgot" id="forgotPassword">Forgot Password</p>
            {{Form::close ()}}
            <div class="popover-container" id="popoverContainer">
              <div class="popover-content" id="popoverContent">
                <button id="close">X</button>
                <p class="popover-message" id="popoverMessage">
                  <div class="popover-row">We will email you a link to reset your password</div>
                  <div class="popover-row">Enter the email you are registered with below</div>
                  {{Form::open (array ('url' => 'forgotPassword'))}}
                  <p class="popover-input"> {{Form::email ('email', '', array ('placeholder'=>'Email','maxlength'=>30))}} </p>
                  <p class="popover-btn"> {{Form::submit ('Submit')}} </p>
                  {{Form::close ()}}
                </p>
              </div>
            </div>
            @if ($errors->any ())
              <div class="errors">
                <h2>Errors</h2>
                 @foreach ($errors->all() as $error)
                    <ul>
                      <li>{{ $error }}</li>
                    </ul>
                @endforeach
              </div>
            @endif

            </div>
      </body>
      <script type="text/javascript">
        let content = document.getElementById("popoverContent");
        document.addEventListener(("click"), (e) => {
          if (e.target.id == "forgotPassword") {
            content.style.display = "block";
          }
          if (e.target.id == "close") {
            content.style.display = "none";
          }
        });
      </script>
</html>
