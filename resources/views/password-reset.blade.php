<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>MNI Search Tool</title>
    </head>
    <body>
      <p>Password Reset</p>
      <p>Click the link to reset your password
          <a href="https://auth.adready.com/password/reset/{{$email}}/{{$token}}">Reset Link</a>
      <p>
    </body>
</html>
